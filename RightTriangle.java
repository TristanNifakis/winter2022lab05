public class RightTriangle {
    private double height;
    private double base;

    public RightTriangle(double h, double b) {
        this.height = h;
        this.base = b;
    }

    public double getArea() {
		return (this.height * this.base)/2;
    }

    public double getPerimeter() {
        return this.height + this.base + this.getHypotenuse();
    }

    public double getHypotenuse() {
        return Math.sqrt(this.height * this.height + this.base * this.base);
    }

    public String toString() {
        return "Height: " + this.height + ", Width: " + this.base + " , Area: " + this.getArea() + " Perimeter: " + this.getPerimeter();
    }
}
