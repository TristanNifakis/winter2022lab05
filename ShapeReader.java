import java.nio.file.*;
import java.util.*;

public class ShapeReader {
    public static void main(String[] args) throws Exception {
       RightTriangle[] triangles = loadTriangles("H:\\Lab_5_Reading_Code\\triangles.csv");
        printTriangles(triangles);
		
		
		RightTriangle triangle1 = new RightTriangle(3,4);
		System.out.println(triangle1);
		
    }

    public static RightTriangle[] loadTriangles(String path) throws Exception {
        List<String> linesAsList = Files.readAllLines(Paths.get(path));
        String[] lines = linesAsList.toArray(new String[0]);

        RightTriangle[] triangles = new RightTriangle[lines.length];
        for (int i = 0; i < lines.length; i++) {
            String[] pieces = lines[i].split(",");
            triangles[i] = new RightTriangle(Double.parseDouble(pieces[0]), Double.parseDouble(pieces[1]));
        }

        return triangles;
    }

    public static void printTriangles(RightTriangle[] triangles) {
        for (RightTriangle triangle : triangles) {
            System.out.println(triangle);
        }
    }
}
